package SERVLET;

import DAO.FactoryProvider;
import DAO.VoterDaImpl;
import ENTITYY.Party;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/AddVoteServlet")
public class AddVoteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");


        long voterId=Long.parseLong(req.getParameter("voterId"));
        String password=req.getParameter("voterPassword");
        int partyId=Integer.parseInt(req.getParameter("partyId"));

        VoterDaImpl voterDaoImpl =new VoterDaImpl();
        int flag=voterDaoImpl.giveVote(voterId,password,partyId);
        if(flag==1)
        {
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("Voter votedd succesfully");
            requestDispatcher.include(req,resp);
        }
        else {
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("VoterId or password wrong or voter has succesfully voted");
            requestDispatcher.include(req,resp);
        }
    }
}

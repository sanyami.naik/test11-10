package SERVLET;

import DAO.FactoryProvider;
import DAO.PartyDaoImpl;
import DAO.VoterDaImpl;
import ENTITYY.Admin;
import ENTITYY.Party;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        String adminPassword=req.getParameter("adminPassword");

        EntityManager entityManager= FactoryProvider.getManagerFactory().createEntityManager();
        Admin admin=entityManager.find(Admin.class,1);

        if(admin.getPassword().equals(adminPassword))
        {
            PartyDaoImpl partyDao=new PartyDaoImpl();
            List<Party> partyList=partyDao.getList();
            for (Party p:partyList) {
                printWriter.println(p+"<br>");
            }
        }
        else {
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("adminLogin.html");
            printWriter.print("Admin Password wrong");
            requestDispatcher.include(req,resp);
        }

    }
}

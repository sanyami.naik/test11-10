package SERVLET;

import DAO.PartyDaoImpl;
import ENTITYY.Party;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/AddPartyServlet")
public class AddPartyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");
        int partyId=Integer.parseInt(req.getParameter("partyId"));
        String partyName=req.getParameter("partyName").trim().toUpperCase();
        String partySymbol=req.getParameter("partySymbol");
        String partySlogan=req.getParameter("partySlogan");

        Party party=new Party(partyId,partyName,partySymbol,partySlogan);
        PartyDaoImpl partyDaoImpl=new PartyDaoImpl();
        int flag=partyDaoImpl.insertParty(party);

        if(flag==1)
        {
            printWriter.print("Party registered succesfully");
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            requestDispatcher.include(req,resp);
        }
        else {
            printWriter.print("Party name cannot be registered or party symbol cannot be registered");
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            requestDispatcher.include(req,resp);
        }
    }
}

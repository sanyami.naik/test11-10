package SERVLET;

import DAO.VoterDaImpl;
import ENTITYY.Voters;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/AddVoterServlet")
public class AddVoterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        String voterName=req.getParameter("voterName");
        String voterPassword=req.getParameter("voterPassword");
        Long voterNumber=Long.parseLong(req.getParameter("voterNumber"));
        int voterAge=Integer.parseInt(req.getParameter("voterAge"));

        Voters voters=new Voters(voterName,voterPassword,voterNumber,voterAge);
        VoterDaImpl voterDaoImpl=new VoterDaImpl();
        int flag=voterDaoImpl.insertVoter(voters);
        if(flag==1)
        {

            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("Voter registered succesfully");
            requestDispatcher.include(req,resp);
        }
        else {
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("Voter already exists cant registered or voter is not 18+");
            requestDispatcher.include(req,resp);
        }
    }
}

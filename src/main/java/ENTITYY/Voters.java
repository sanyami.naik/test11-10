package ENTITYY;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Voters {
    @Id
    long voterId;
    String voterName;
    String voterPassword;

    int voterAge;
    long voterNumber;

    public Voters() {

    }

    public int getVoterAge() {
        return voterAge;
    }

    public void setVoterAge(int voterAge) {
        this.voterAge = voterAge;
    }

    public Voters(String voterName, String voterPassword, long voterNumber,int voterAge) {
        this.voterName = voterName;
        this.voterPassword = voterPassword;
        this.voterNumber = voterNumber;
        this.voterAge=voterAge;
    }

    @ManyToOne
    Party party;

    public long getVoterId() {
        return voterId;
    }

    public void setVoterId(long voterId) {
        this.voterId = voterId;
    }

    public String getVoterName() {
        return voterName;
    }

    public void setVoterName(String voterName) {
        this.voterName = voterName;
    }

    public String getVoterPassword() {
        return voterPassword;
    }

    public void setVoterPassword(String voterPassword) {
        this.voterPassword = voterPassword;
    }

    public long getVoterNumber() {
        return voterNumber;
    }

    public void setVoterNumber(long voterNumber) {
        this.voterNumber = voterNumber;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }
}

package ENTITYY;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Party {
    @Id
    int partyId;
    String partyName;
    String partySymbol;
    String partySlogan;
    int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @OneToMany(mappedBy = "party")
    List<Voters> votersList;

    public Party() {
    }

    public Party(int partyId, String partyName, String partySymbol, String partySlogan) {
        this.partyId = partyId;
        this.partyName = partyName;
        this.partySymbol = partySymbol;
        this.partySlogan = partySlogan;
    }

    public int getPartyId() {
        return partyId;
    }

    public void setPartyId(int partyId) {
        this.partyId = partyId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartySymbol() {
        return partySymbol;
    }

    public void setPartySymbol(String partySymbol) {
        this.partySymbol = partySymbol;
    }

    public String getPartySlogan() {
        return partySlogan;
    }

    public void setPartySlogan(String partySlogan) {
        this.partySlogan = partySlogan;
    }

    public List<Voters> getVotersList() {
        return votersList;
    }

    public void setVotersList(List<Voters> votersList) {
        this.votersList = votersList;
    }

    @Override
    public String toString() {
        return "Party{" +
                "partyId=" + partyId +
                ", partyName='" + partyName + '\'' +
                ", partySymbol='" + partySymbol + '\'' +
                ", partySlogan='" + partySlogan + '\'' +
                ", count=" + count +
                '}';
    }
}

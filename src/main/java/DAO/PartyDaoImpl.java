package DAO;

import ENTITYY.Party;
import ENTITYY.Voters;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PartyDaoImpl {

    public int insertParty(Party party)
    {
        int flag=0;
        EntityManager entityManager=FactoryProvider.getManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<Party> root= criteriaQuery.from(Party.class);

        Query query= entityManager.createQuery(criteriaQuery.select(root));
        List<Party> partyList=query.getResultList();

        long count=partyList.stream().filter(e-> Arrays.stream(e.getPartySymbol().split(" ")).iterator().next().contains(Arrays.stream(party.getPartyName().split(" ")).iterator().next())).collect(Collectors.toList()).size();

        long nCount=partyList.stream().filter(e->e.getPartyName().contains(party.getPartyName())).count();
        long sCount=partyList.stream().filter(e->e.getPartySymbol().contains(party.getPartySymbol())).count();

        if(nCount==0 && sCount==0) {
            entityManager.getTransaction().begin();
            entityManager.persist(party);
            entityManager.getTransaction().commit();
            flag=1;
        }


        partyList.stream().filter(e->e.getPartySymbol().equals(party.getPartySymbol()));

        entityManager.close();
        return flag;
    }

    public List<Party> getList()
    {
        EntityManager entityManager=FactoryProvider.getManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<Party> root= criteriaQuery.from(Party.class);

        Query query= entityManager.createQuery(criteriaQuery.select(root));
        return query.getResultList();
    }
}

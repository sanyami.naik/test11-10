package DAO;

import ENTITYY.Admin;
import ENTITYY.Party;
import ENTITYY.Voters;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import java.util.List;
import java.util.Random;

public class VoterDaImpl {

    public int insertVoter(Voters voters)
    {
        int flag=0;
        EntityManagerFactory entityManagerFactory=FactoryProvider.getManagerFactory();
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<Voters> root= criteriaQuery.from(Voters.class);

        Query query= entityManager.createQuery(criteriaQuery.select(root));
        List<Voters> votersList=query.getResultList();

        long num=votersList.stream().filter(e->e.getVoterNumber()==voters.getVoterNumber()).count();

        if(num==0 && voters.getVoterAge()>18) {

            Random random=new Random();
            int number=random.nextInt(100000000-10000000)+10000000;
            int r=random.nextInt(1000000000);

            voters.setVoterId(number);
            entityManager.getTransaction().begin();
            entityManager.persist(voters);
            entityManager.getTransaction().commit();
            flag=1;
        }else
        {
          flag=0;
        }
        entityManager.close();
        return flag;
    }

    public int giveVote(long voteId,String password,int partyId)
    {
        int flag=0;

        EntityManagerFactory entityManagerFactory=FactoryProvider.getManagerFactory();
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<Voters> root= criteriaQuery.from(Voters.class);

        Voters voters=entityManager.find(Voters.class,voteId);
        if(voters!=null && voters.getParty()==null)
        {
            String givenPassword= password;
            String tablePassword=voters.getVoterPassword();
            if(givenPassword.equals(tablePassword))
            {
                entityManager.getTransaction().begin();
                Party party=entityManager.find(Party.class,partyId);
                voters.setParty(party);
                party.setCount(party.getCount()+1);
                entityManager.getTransaction().commit();

                flag=1;
            }

        }
        entityManager.close();
        return flag;
    }




}

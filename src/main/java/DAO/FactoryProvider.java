package DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FactoryProvider {

    public static EntityManagerFactory entityManagerFactory;

    public static EntityManagerFactory getManagerFactory()
    {


        if (entityManagerFactory==null)
        {
            entityManagerFactory= Persistence.createEntityManagerFactory("voting");
        }
        return entityManagerFactory;
    }
}
